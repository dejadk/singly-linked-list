#pragma once
#include <stdlib.h>

struct node {
    void *data;
    struct node *next;
};

struct node *list_create(void);
void list_append(struct node *head, void *data);
void list_remove(struct node *head, void *data);
struct node *list_find(struct node *head, void *data);
void list_destroy(struct node *head);

struct node *list_create(void)
{
    struct node *head = malloc(sizeof(*head));
    if (head) {
        head->data = NULL;
        head->next = NULL;
    }

    return head;
}

void list_append(struct node *head, void *data)
{
    struct node *curr = head;

    while (curr->next) {
        curr = curr->next;
    }

    struct node *new_node = malloc(sizeof(*new_node));
    if (new_node) {
        new_node->data = data;
        new_node->next = NULL;
    }

    curr->next = new_node;
}

void list_remove(struct node *head, void *data)
{
    struct node *curr = head;
    struct node *prev = NULL;

    while (curr) {
        if (curr->data == data)
            break;

        prev = curr;
        curr = curr->next;
    }

    if (prev && curr)
        prev->next = curr->next;

    free(curr);
}

struct node *list_find(struct node *head, void *data)
{
    struct node *curr = head;
    struct node *found = NULL;

    while (curr) {
        if (curr->data == data) {
            found = curr;
            break;
        }

        curr = curr->next;
    }

    return found;
}

void list_destroy(struct node *head)
{
    struct node *curr = head;
    struct node *next = NULL;

    while (curr) {
        next = curr->next;
        free(curr);
        curr = next;
    }
}
